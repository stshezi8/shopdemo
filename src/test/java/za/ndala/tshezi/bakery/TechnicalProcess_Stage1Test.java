package za.ndala.tshezi.bakery;

import org.junit.jupiter.api.Test;
import za.ndala.tshezi.bakery.common.Bread;
import za.ndala.tshezi.bakery.common.Transaction;
import za.ndala.tshezi.bakery.common.business.Change;
import za.ndala.tshezi.bakery.common.business.SaleResponse;
import za.ndala.tshezi.bakery.common.business.SalesRequest;
import za.ndala.tshezi.bakery.common.business.SalesTransaction;
import za.ndala.tshezi.bakery.customer.Purchase;
import za.ndala.tshezi.bakery.transaction.DirectSale;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TechnicalProcess_Stage1Test {

    // customer: purchase   >  payment amount, quantity of items
    // business: sale       > item price
    // sale requirements: customer and business in transaction

    void validateCustomerRequest() {
        // customer 1: 1 loaf, R 10.50
        // customer 2: 1 loaf, R 9.00
        // customer 3: 1 loaf, R 19.00
        // customer 4: 2 loaves, 2 half loaves, R 50.00
    }

   /* @Test
    void whenRequestForLoaf_thenValidateAmountEqualToItemPrice() {
        returnProgrammedBusinessResponse();

        var halfLoaves = 1;
        var fullLoaves = 1;
        var paymentAmount = BigDecimal.valueOf(10.50);
        var purchase = new Purchase(halfLoaves, fullLoaves, paymentAmount);

        Transaction transaction = new DirectSale();
        transaction.process(purchase);
    }

    private void returnProgrammedBusinessResponse() {
        // business
        java.util.List<Bread> bread = new ArrayList<>();
        Change change = new Change(BigDecimal.ZERO);
        var saleResponse = new SaleResponse(bread, change);

        SalesTransaction saleRequestedInterface = mock(SalesTransaction.class);
        SalesRequest salesRequest = mock(SalesRequest.class);
        when(
                saleRequestedInterface.sale(salesRequest)
        ).thenReturn(saleResponse);
    }*/
}
