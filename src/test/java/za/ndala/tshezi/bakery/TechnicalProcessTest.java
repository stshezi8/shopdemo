package za.ndala.tshezi.bakery;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import za.ndala.tshezi.bakery.common.Bread;
import za.ndala.tshezi.bakery.common.HalfLoaf;
import za.ndala.tshezi.bakery.common.Loaf;
import za.ndala.tshezi.bakery.common.Transaction;
import za.ndala.tshezi.bakery.common.business.Change;
import za.ndala.tshezi.bakery.common.business.SaleResponse;
import za.ndala.tshezi.bakery.common.business.SalesRequest;
import za.ndala.tshezi.bakery.common.business.SalesTransaction;
import za.ndala.tshezi.bakery.customer.Purchase;
import za.ndala.tshezi.bakery.transaction.DirectSale;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TechnicalProcessTest {

    private static final BigDecimal HALF_LOAF_PRICE = new BigDecimal("9.00");
    private static final BigDecimal FULL_LOAF_PRICE = new BigDecimal("10.50");

    @Test
    @SuppressWarnings("ConstantConditions")
    void paymentAmountIsRequired() {

        // SETUP
        var halfLoafQuantity = 1;
        var fullLoafQuantity = 1;
        BigDecimal paymentAmount = null;
        var purchaseWithoutPaymentAmount = new Purchase(
                halfLoafQuantity, fullLoafQuantity, paymentAmount
        );

        Transaction transaction = new DirectSale();

        // TEST && VERIFY
        Assertions.assertThrows(
                // VERIFICATION
                IllegalStateException.class,
                // TEST
                () -> transaction.process(purchaseWithoutPaymentAmount)
        );
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void whenPaymentAmountNotProvided_reportReasonToCustomer() {

        // SETUP
        var halfLoafQuantity = 1;
        var fullLoafQuantity = 1;
        BigDecimal paymentAmount = null;
        var purchaseWithoutPaymentAmount = new Purchase(
                halfLoafQuantity, fullLoafQuantity, paymentAmount
        );

        Transaction transaction = new DirectSale();

        // TEST && VERIFY
        Throwable customerError = Assertions.assertThrows(
                // VERIFICATION
                IllegalStateException.class,
                // TEST
                () -> transaction.process(purchaseWithoutPaymentAmount)
        );

        String expectedMessage = "Payment Amount was not provided, you cannot purchase without it";
        assertEquals(expectedMessage, customerError.getMessage());
    }

    @Test
    void whenPaymentAmountNotPositiveValue_reportReasonToCustomer() {
        // SETUP
        var paymentAmount = BigDecimal.ZERO;
        var halfLoafQuantity = 1;
        var fullLoafQuantity = 1;
        var purchaseWithNegativePaymentAmount = new Purchase(
                halfLoafQuantity, fullLoafQuantity, paymentAmount
        );

        Transaction transaction = new DirectSale();

        try {
            // TEST
            transaction.process(purchaseWithNegativePaymentAmount);

            Assertions.fail("Expected an error due to negative payment amount provided");
        } catch (Exception e) {
            // VERIFY
            assertInstanceOf(IllegalStateException.class, e);
            var expectedMessage = "Purchasing bread requires valid payment amount";
            assertEquals(expectedMessage, e.getMessage());
        }
    }

    @Test
    void whenPaymentAmountIsValid_theNoErrorMessageIsReported() {
        // SETUP
        var paymentAmount = BigDecimal.TEN;
        var halfLoafQuantity = 1;
        var fullLoafQuantity = 1;
        var purchaseWithPositivePaymentAmount = new Purchase(
                halfLoafQuantity, fullLoafQuantity, paymentAmount
        );
        Transaction transaction = new DirectSale();

        // TEST
        try {
            var saleResponse = transaction.process(purchaseWithPositivePaymentAmount);

            // VERIFY
            Assertions.assertNotNull(saleResponse);
        } catch (Exception e) {
            Assertions.fail("No error must be reported when payment amount is valid");
        }
    }

    //Scenario 1
    @Test
    void canBuyHalfLoafWithoutChange() {
        // SETUP
        BigDecimal paymentAmount = HALF_LOAF_PRICE;
        int halfLoafQuantity = 1;
        int fullLoafQuantity = 0; // No full loaf
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        HalfLoaf halfLoaf = (HalfLoaf) breadList.get(0); // Assuming there's only one item in the list

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(1, breadList.size()); // Expecting one item in the list
        Assertions.assertEquals(halfLoafQuantity, halfLoaf.getHalfLoafCount());
    }

    // Scenario 2
    @Test
    void canBuyHalfLoafWithExactChange() {
        // SETUP
        BigDecimal paymentAmount = HALF_LOAF_PRICE;
        int halfLoafQuantity = 1;
        int fullLoafQuantity = 0; // No full loaf
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        HalfLoaf halfLoaf = (HalfLoaf) breadList.get(0);

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(1, breadList.size());
        Assertions.assertEquals(halfLoafQuantity, halfLoaf.getHalfLoafCount());
        Assertions.assertEquals(0, halfLoaf.getFullLoafCount());
    }

    // scenarios3

    @Test
    void canBuyFullLoafWithExactChange() {
        // SETUP
        BigDecimal paymentAmount = FULL_LOAF_PRICE;
        int halfLoafQuantity = 0; // No half loaf
        int fullLoafQuantity = 1;
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        Loaf loaf = (Loaf) breadList.get(0); // Assuming there's only one item in the list

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(1, breadList.size()); // Expecting one item in the list
        Assertions.assertEquals(0, loaf.getHalfLoafCount());
        Assertions.assertEquals(fullLoafQuantity, loaf.getFullLoafCount());
    }

    // Scenario 4
    @Test
    void canBuyLoafWithChange() {
        // SETUP
        BigDecimal paymentAmount = FULL_LOAF_PRICE.add(BigDecimal.ONE);
        int halfLoafQuantity = 0; // No half loaf
        int fullLoafQuantity = 1;
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        Loaf loaf = (Loaf) breadList.get(0); // Assuming there's only one item in the list

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(1, breadList.size()); // Expecting one item in the list
        Assertions.assertEquals(0, loaf.getHalfLoafCount());
        Assertions.assertEquals(fullLoafQuantity, loaf.getFullLoafCount());
    }

    // Scenario 5
    @Test
    void canBuyBothWithExactChange() {
        // SETUP
        BigDecimal paymentAmount = HALF_LOAF_PRICE.add(FULL_LOAF_PRICE);
        int halfLoafQuantity = 1;
        int fullLoafQuantity = 1;
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        HalfLoaf halfLoaf = null;
        Loaf loaf = null;
        for (Bread bread : breadList) {
            if (bread instanceof HalfLoaf) {
                halfLoaf = (HalfLoaf) bread;
            } else if (bread instanceof Loaf) {
                loaf = (Loaf) bread;
            }
        }

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(2, breadList.size()); // Expecting two items in the list
        Assertions.assertNotNull(halfLoaf);
        Assertions.assertNotNull(loaf);
        Assertions.assertEquals(1, halfLoaf.getHalfLoafCount());
        Assertions.assertEquals(0, halfLoaf.getFullLoafCount());
        Assertions.assertEquals(0, loaf.getHalfLoafCount());
        Assertions.assertEquals(1, loaf.getFullLoafCount());
    }

    @Test
    void canBuyBothWithChange() {
        // SETUP
        BigDecimal paymentAmount = HALF_LOAF_PRICE.add(FULL_LOAF_PRICE).add(BigDecimal.ONE);
        int halfLoafQuantity = 1;
        int fullLoafQuantity = 1;
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        HalfLoaf halfLoaf = null;
        Loaf loaf = null;
        for (Bread bread : breadList) {
            if (bread instanceof HalfLoaf) {
                halfLoaf = (HalfLoaf) bread;
            } else if (bread instanceof Loaf) {
                loaf = (Loaf) bread;
            }
        }

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(2, breadList.size());
        Assertions.assertNotNull(halfLoaf);
        Assertions.assertNotNull(loaf);
        Assertions.assertEquals(1, halfLoaf.getHalfLoafCount());
        Assertions.assertEquals(0, halfLoaf.getFullLoafCount());
        Assertions.assertEquals(0, loaf.getHalfLoafCount());
        Assertions.assertEquals(1, loaf.getFullLoafCount());
    }

    // Scenario 7

    @Test
    void canBuyMultipleHalfLoavesAndOneLoaf() {
        // SETUP
        BigDecimal paymentAmount = HALF_LOAF_PRICE.multiply(BigDecimal.valueOf(2)).add(FULL_LOAF_PRICE); // Payment for 2 half loaves and 1 full loaf
        int halfLoafQuantity = 2; // Buy 2 half loaves
        int fullLoafQuantity = 1; // Buy 1 full loaf
        Transaction transaction = new DirectSale();
        Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

        // TEST
        SaleResponse saleResponse = transaction.process(purchase);
        List<Bread> breadList = saleResponse.getBread();
        int halfLoafCount = 0;
        int fullLoafCount = 0;
        for (Bread bread : breadList) {
            if (bread instanceof HalfLoaf) {
                halfLoafCount++;
            } else if (bread instanceof Loaf) {
                fullLoafCount++;
            }
        }

        // VERIFY
        Assertions.assertNotNull(breadList);
        Assertions.assertEquals(3, breadList.size());
        Assertions.assertEquals(2, halfLoafCount);
        Assertions.assertEquals(1, fullLoafCount);
    }

// Scenario8
@Test
void canBuyMultipleHalfLoavesAndMultipleLoaves() {
    // SETUP
    BigDecimal paymentAmount = HALF_LOAF_PRICE.multiply(BigDecimal.valueOf(2))
            .add(FULL_LOAF_PRICE.multiply(BigDecimal.valueOf(2))); // Payment for 2 half loaves and 2 full loaves
    int halfLoafQuantity = 2; // Buy 2 half loaves
    int fullLoafQuantity = 2; // Buy 2 full loaves
    Transaction transaction = new DirectSale();
    Purchase purchase = new Purchase(halfLoafQuantity, fullLoafQuantity, paymentAmount);

    // TEST
    SaleResponse saleResponse = transaction.process(purchase);
    List<Bread> breadList = saleResponse.getBread();
    int halfLoafCount = 0;
    int fullLoafCount = 0;
    for (Bread bread : breadList) {
        if (bread instanceof HalfLoaf) {
            halfLoafCount++;
        } else if (bread instanceof Loaf) {
            fullLoafCount++;
        }
    }

    // VERIFY
    Assertions.assertNotNull(breadList);
    Assertions.assertEquals(4, breadList.size());
    Assertions.assertEquals(2, halfLoafCount);
    Assertions.assertEquals(2, fullLoafCount);
}


}
/*
* Transaction that validate purchases request....
* Let's write unit test about to test HalfLoaf and Loaf ,
* scenarios1  you can buy HalfLoaf only with amount that doesn't want change
* scenarios2  you can buy HalfLoaf only with amount that want change
 * scenarios3  you can buy Loaf only with amount that doesn't want change
 * scenarios4  you can buy Loaf only with amount that want change
 * scenarios5 you can buy both(HalfLoaf and Loaf) with amount that doesn't want change
 * scenarios6 you can buy both(HalfLoaf and Loaf) with amount that  want change
 * scenarios7 you can buy more than 1 HalfLoaf (let's say 2) and only one Loaf
 * Scenarios8 you can buy more than 1 HalfLoaf (let's say 2) and you can buy more than 1  Loaf(let's say 2)
 * Let's say price of HalfLoaf = 9.00
* Let's say Loaf = 10.50
*
* */