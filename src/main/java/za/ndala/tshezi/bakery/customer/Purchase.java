package za.ndala.tshezi.bakery.customer;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class Purchase {
    private int halfLoafQuantity;
    private int fullLoafQuantity;
    private BigDecimal paymentAmount;
}
