package za.ndala.tshezi.bakery.transaction;

import za.ndala.tshezi.bakery.common.Bread;
import za.ndala.tshezi.bakery.common.HalfLoaf;
import za.ndala.tshezi.bakery.common.Loaf;
import za.ndala.tshezi.bakery.common.business.Change;
import za.ndala.tshezi.bakery.common.business.SaleResponse;
import za.ndala.tshezi.bakery.common.Transaction;
import za.ndala.tshezi.bakery.customer.Purchase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DirectSale implements Transaction {
    String CUSTOMER_ERROR_WHEN_NO_PAYMENT_AMOUNT
            = "Payment Amount was not provided, you cannot purchase without it";
    String CUSTOMER_ERROR_PAYMENT_AMOUNT_HAS_NO_VALUE
            = "Purchasing bread requires valid payment amount";

    @Override
    public SaleResponse process(Purchase purchase) {
        if (purchase.getPaymentAmount() == null)
            throw new IllegalStateException(CUSTOMER_ERROR_WHEN_NO_PAYMENT_AMOUNT);

        if (purchase.getPaymentAmount().compareTo(BigDecimal.ZERO) == 0)
            throw new IllegalStateException(CUSTOMER_ERROR_PAYMENT_AMOUNT_HAS_NO_VALUE);


        List<Bread> breadList = createBreadList(purchase);
        Change change = calculateChange(purchase);

        return new SaleResponse(breadList, change);

    }

    // Example method to create a list of Bread objects based on the purchase
    private List<Bread> createBreadList(Purchase purchase) {
        List<Bread> breadList = new ArrayList<>();

        int halfLoafQuantity = purchase.getHalfLoafQuantity();
        int fullLoafQuantity = purchase.getFullLoafQuantity();

        // Create HalfLoaf objects
        for (int i = 0; i < halfLoafQuantity; i++) {
            breadList.add(new HalfLoaf(1));
        }

        // Create Loaf objects
        for (int i = 0; i < fullLoafQuantity; i++) {
            breadList.add(new Loaf(0, 1));
        }

        return breadList;
    }

    private Change calculateChange(Purchase purchase) {
        BigDecimal halfLoafPrice = new BigDecimal("4.50");
        BigDecimal fullLoafPrice = new BigDecimal("5.25");

        BigDecimal totalHalfLoafAmount = halfLoafPrice.multiply(new BigDecimal(purchase.getHalfLoafQuantity()));
        BigDecimal totalFullLoafAmount = fullLoafPrice.multiply(new BigDecimal(purchase.getFullLoafQuantity()));
        BigDecimal totalAmount = totalHalfLoafAmount.add(totalFullLoafAmount);

        BigDecimal changeAmount = purchase.getPaymentAmount().subtract(totalAmount);

        return new Change(changeAmount);
    }
}
