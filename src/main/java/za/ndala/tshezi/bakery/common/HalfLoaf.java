package za.ndala.tshezi.bakery.common;

import za.ndala.tshezi.bakery.common.Bread;

public class HalfLoaf implements Bread {
    private final int halfLoafCount;

    public HalfLoaf(int halfLoafCount) {
        this.halfLoafCount = halfLoafCount;
    }

    public int getHalfLoafCount() {
        return halfLoafCount;
    }
    public int getFullLoafCount() {
        return 0;
    }
}
