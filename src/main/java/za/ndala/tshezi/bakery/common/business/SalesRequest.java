package za.ndala.tshezi.bakery.common.business;

public record SalesRequest(Payment amount, Quantity quantity) {
}
