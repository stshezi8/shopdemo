package za.ndala.tshezi.bakery.common.business;

public record Payment(Money amount) {
}
