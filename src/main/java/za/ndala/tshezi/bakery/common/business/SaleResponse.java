package za.ndala.tshezi.bakery.common.business;

import za.ndala.tshezi.bakery.common.Bread;

import java.math.BigDecimal;
import java.util.List;

public record SaleResponse(List<Bread> bread, Change change) {
    public int getLoaves() {
        return 0;
    }

    public BigDecimal getChange() {
        return null;
    }
    public List<Bread> getBread() {
        return bread;
    }
}
