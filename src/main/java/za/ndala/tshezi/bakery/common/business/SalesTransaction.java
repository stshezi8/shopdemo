package za.ndala.tshezi.bakery.common.business;


public interface SalesTransaction {
    Object sale(SalesRequest request);
}
