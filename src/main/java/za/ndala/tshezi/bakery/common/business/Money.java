package za.ndala.tshezi.bakery.common.business;

import java.math.BigDecimal;

public record Money(BigDecimal amount) {
}
