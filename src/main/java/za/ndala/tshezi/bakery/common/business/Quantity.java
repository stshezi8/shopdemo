package za.ndala.tshezi.bakery.common.business;

import za.ndala.tshezi.bakery.common.Loaf;

import java.util.List;

public record Quantity(List<Loaf> loaves) {
}
