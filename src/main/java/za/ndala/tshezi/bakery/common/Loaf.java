package za.ndala.tshezi.bakery.common;


public class Loaf implements Bread {
    private final int halfLoafCount;
    private final int fullLoafCount;

    public Loaf(int halfLoafCount, int fullLoafCount) {
        this.halfLoafCount = halfLoafCount;
        this.fullLoafCount = fullLoafCount;
    }

    public int getHalfLoafCount() {
        return halfLoafCount;
    }

    public int getFullLoafCount() {
        return fullLoafCount;
    }
}
