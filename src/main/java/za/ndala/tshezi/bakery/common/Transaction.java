package za.ndala.tshezi.bakery.common;

import za.ndala.tshezi.bakery.common.business.SaleResponse;
import za.ndala.tshezi.bakery.customer.Purchase;

public interface Transaction {
    SaleResponse process(Purchase purchase);
}
